import 'package:build/build.dart';
import 'src/builder/nodejs_builder.dart';

/// Builder used to compile dart to NodeJS compatible using dart2js.
Builder nodeJsBuilder(BuilderOptions options) =>
    NodeJsBuilder.fromOptions(options);
