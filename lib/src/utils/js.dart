// ignore_for_file: public_member_api_docs
library firebase_node.utils.js;

/// This class is a wrapper for the jsObject. All the specific JsObject
/// wrappers extend from it.
abstract class JsObjectWrapper<T> {
  /// JS object.
  final T jsObject;

  /// Creates a new JsObjectWrapper type from a [jsObject].
  const JsObjectWrapper.fromJsObject(this.jsObject);
}
