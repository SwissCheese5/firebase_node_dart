import 'package:js/js_util.dart' as util;

import 'js_interop.dart' as js;

/// Extensien to help manage jsObject
extension JsUtils on dynamic {
  /// check whether jsObject has keys with given values
  bool hasProperties(List<String> properties) =>
      properties.every((element) => util.hasProperty(this, element));

  /// retrieve current keys of jsObject
  List<String> get jsKeys => js.objectKeys(this);

  /// retrive current value of given jsObject key
  dynamic getProperty(String property) => util.getProperty(this, property);
}
