// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.interop.es6_interop;

import 'package:js/js.dart';

@JS('Promise')
class PromiseJsImpl<T> {
  external PromiseJsImpl(Function resolver);
  external PromiseJsImpl then([
    dynamic Function(dynamic) onResolve,
    dynamic Function(dynamic) onReject,
  ]);
}
