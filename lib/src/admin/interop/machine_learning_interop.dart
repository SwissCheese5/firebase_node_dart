// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.admin.interop.machine_learning;

import 'package:js/js.dart';

import '../../utils/es6_interop.dart';
import 'app_interop.dart';

/// Interface representing options for listing Models.
@JS()
@anonymous
abstract class ListModelsOptions {
  /// Interface representing options for listing Models.
  ///
  /// - Optional `filter`
  /// An expression that specifies how to filter the results.
  ///
  /// Examples:
  ///
  /// display_name = your_model
  /// display_name : experimental_*
  /// tags: face_detector AND tags: experimental
  /// state.published = true
  ///
  /// See https://firebase.google.com/docs/ml-kit/manage-hosted-models#list_your_projects_models
  ///
  /// - Optional `pageSize`
  /// The number of results to return in each page.
  ///
  /// - Optional `pageToken`
  /// A token that specifies the result page to return
  ///   String get pageToken;
  external factory ListModelsOptions({
    String filter,
    String pageToken,
    String pageSize,
  });

  /// Optional filter
  /// filter: undefined | string
  /// An expression that specifies how to filter the results.
  ///
  /// Examples:
  ///
  /// display_name = your_model
  /// display_name : experimental_*
  /// tags: face_detector AND tags: experimental
  /// state.published = true
  ///
  /// See https://firebase.google.com/docs/ml-kit/manage-hosted-models#list_your_projects_models
  external String get filter;
  external set filter(String v);

  /// Optional pageSize
  /// pageSize: undefined | number
  /// The number of results to return in each page.
  external String get pageSize;
  external set pageSize(String v);

  /// Optional pageToken
  /// pageToken: undefined | string
  /// A token that specifies the result page to return
  ///   String get pageToken;
  external String get pageToken;
  external set pageToken(String v);
}

@JS('ListModelsResult')
@anonymous
abstract class ListModelsResultJsImpl {
  external List get models;
  external String get pageToken;
}

@JS('MachineLearning')
@anonymous
abstract class MachineLearningJsImpl {
  external AppJsImpl get app;
  external PromiseJsImpl<void> createModel(ModelOptionsJsImpl model);
  external PromiseJsImpl<void> deleteModel(String modelId);
  external PromiseJsImpl<ModelJsImpl> getModel(String modelId);
  external PromiseJsImpl<ListModelsResultJsImpl> listModels(
      ListModelsOptions options);
  external PromiseJsImpl<ModelJsImpl> publishModel(String modelId);
  external PromiseJsImpl<ModelJsImpl> unpublishModel(String modelId);
  external PromiseJsImpl<ModelJsImpl> updateModel(
      String modelId, ModelOptionsJsImpl model);
}

@JS('Model')
@anonymous
abstract class ModelJsImpl {
  external String get createTime;
  external String get displayName;
  external String get etag;
  external bool get locked;
  external String get modelHash;
  external String get modelId;
  external bool get published;
  external List<String> get tags;
  external TFLiteModel get tfliteModel;
  external String get updateTime;
  external String get validationError;
  external PromiseJsImpl<void> waitForUnlocked([num maxTimeSeconds]);
}

@JS('ModelOptions')
@anonymous
abstract class ModelOptionsJsImpl {
  external factory ModelOptionsJsImpl({
    String displayName,
    List<String> tags,
    TFLiteModel tfliteModel,
  });
  external String get displayName;
  external set displayName(String v);
  external List<String> get tags;
  external set tags(List<String> v);
  external TFLiteModel get tfliteModel;
  external set tfliteModel(TFLiteModel v);
}

/// A TensorFlow Lite Model output object
@JS()
@anonymous
abstract class TFLiteModel {
  /// The URI from which the model was originally provided to Firebase.
  external String get gcsTfliteUri;

  /// The size of the model.
  external String get sizeBytes;
}
