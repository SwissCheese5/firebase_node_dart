part of '../../admin.dart';

/// A Firebase App holds the initialization information for a collection
/// of services.
///
/// See: <https://firebase.google.com/docs/reference/js/firebase.app>.
class App extends JsObjectWrapper<app_interop.AppJsImpl> {
  static final _expando = Expando<App>();

  App._fromJsObject(app_interop.AppJsImpl jsObject)
      : super.fromJsObject(jsObject);

  /// Name of the app.
  String get name => jsObject.name;

  /// Options used during [firebase.initializeApp()].
  AppOptions get options => AppOptions.getInstance(jsObject.options);

  /// Returns [Auth] service.
  Auth auth() => Auth.getInstance(jsObject.auth());

  /// Returns [Database] service.
  Database database() => Database.getInstance(jsObject.database());

  /// Deletes the app and frees resources of all App's services.
  Future<void> delete() => handleThenable(jsObject.delete());

  /// Returns [Firestore] service.
  Firestore firestore() => Firestore.getInstance(jsObject.firestore());

  /// Returns [InstanceId] service.
  InstanceId instanceId() => InstanceId.getInstance(jsObject.instanceId());

  /// Returns [MachineLearning] service.
  MachineLearning machineLearning() =>
      MachineLearning.getInstance(jsObject.machineLearning());

  /// Returns [Messaging] service.
  Messaging messaging() => Messaging.getInstance(jsObject.messaging());

  /// Returns [ProjectManagement] service.
  ProjectManagement projectManagement() =>
      ProjectManagement.getInstance(jsObject.projectManagement());

  /// Returns [RemoteConfig] service.
  RemoteConfig remoteConfig() =>
      RemoteConfig.getInstance(jsObject.remoteConfig());

  /// Returns [SecurityRules] service.
  SecurityRules securityRules() =>
      SecurityRules.getInstance(jsObject.securityRules());

  /// Creates a new App from a [jsObject].
  static App getInstance(app_interop.AppJsImpl jsObject) {
    if (jsObject == null) return null;

    return _expando[jsObject] ??= App._fromJsObject(jsObject);
  }
}
