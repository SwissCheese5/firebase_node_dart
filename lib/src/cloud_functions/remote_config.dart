import 'package:js/js.dart';

import '../utils/js.dart';
import 'functions.dart';
import 'interop/remote_config_interop.dart' as interop;

export 'interop/remote_config_interop.dart'
    show RemoteConfigUser, TemplateVersion;

///Construct remote config functions
class RemoteConfigFunctionsBuilder
    extends JsObjectWrapper<interop.RemoteConfigFunctionsBuilderJsImpl> {
  ///Construct remote config functions
  RemoteConfigFunctionsBuilder.fromJsObject(
      interop.RemoteConfigFunctionsBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Handle all updates (including rollbacks) that affect a Remote Config
  ///project.
  dynamic onUpdate(CloudFunction<interop.TemplateVersion> handler) {
    return jsObject.onUpdate(allowInterop((dataJs, contextJs) {
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(dataJs, context, handler);
    }));
  }
}
