// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.functions.interop.auth;

import 'package:js/js.dart';

import '../../admin/interop/auth_interop.dart';
import 'functions_interop.dart';

@JS()
@anonymous
abstract class AuthFunctionsBuilderJsImpl {
  ///Handle events related to Firebase authentication users.
  external UserBuilderJsImpl user();
}

@JS()
@anonymous
abstract class UserBuilderJsImpl {
  ///Respond to the creation of a Firebase Auth user.
  external dynamic onCreate(JsHandler<UserRecordJsImpl> handler);

  ///Respond to the deletion of a Firebase Auth user.
  external dynamic onDelete(JsHandler<UserRecordJsImpl> handler);
}

@JS()
@anonymous
abstract class UserRecordMetadata {
  external String get creationTime;

  external String get lastSignInTime;
}
