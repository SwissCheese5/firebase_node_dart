## 0.1.3

- Fix `listCollections()`
- Add some storage features
- Change factory to static method

## 0.1.2+3

- Change Dart SDK to 2.8.1

## 0.1.2+2

- Fix typo

## 0.1.2

- Add some docs
- Add some admin storage features
- Fixed some issues

## 0.1.1

- Some admin storage features completed
- Change factory constructor to static if it can return null

## 0.1.0

-	Some bug fixes
- Admin Storage on progress

## 0.0.4

- Implement getAll in firebase
- Fix firebase transaction get
- Add transaction getDocument and getQuery

## 0.0.3

- Create custom builder that allow to require NodeJS modules
- Fix update methods in Firestore classes

## 0.0.2

- Fix admin.initializeApp() on null options
- Add example
- Fix pedantic and pana

## 0.0.1

- Initial version
